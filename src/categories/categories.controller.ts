import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CreateCategoryDTO } from './dtos/create-category.dto';
import { Category } from './interfaces/category.interface';
import { CategoriesService } from './categories.service';
import { UpdateCategoryDTO } from './dtos/update-category.dto';

@Controller('api/v1/categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @Post()
  @UsePipes(ValidationPipe)
  async createCategory(
    @Body() createCategoryDTO: CreateCategoryDTO,
  ): Promise<Category> {
    return await this.categoriesService.createCategory(createCategoryDTO);
  }

  @Get()
  async findCategories(): Promise<Array<Category>> {
    return await this.categoriesService.findCategories();
  }

  @Get('/:id')
  async findCategoryById(@Param('id') categoryId: string): Promise<Category> {
    return await this.categoriesService.findCategoryById(categoryId);
  }

  @Put('/:id')
  @UsePipes(ValidationPipe)
  async updateCategory(
    @Body() updateCategoryDTO: UpdateCategoryDTO,
    @Param('id') id: string,
  ): Promise<void> {
    return await this.categoriesService.updateCategory(id, updateCategoryDTO);
  }

  @Post(':category/players/:playerId')
  async addPlayerCategory(@Param() params: string[]): Promise<void> {
    return await this.categoriesService.addPlayerToCategory(params);
  }
}
