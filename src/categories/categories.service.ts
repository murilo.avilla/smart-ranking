import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Model } from 'mongoose';
import { Category } from './interfaces/category.interface';
import { CreateCategoryDTO } from './dtos/create-category.dto';
import { UpdateCategoryDTO } from './dtos/update-category.dto';
import { PlayersService } from 'src/players/players.service';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectModel('Category') private readonly categoryModel: Model<Category>,
    private playerService: PlayersService,
  ) {}

  async createCategory(
    createCategoryDTO: CreateCategoryDTO,
  ): Promise<Category> {
    const { category } = createCategoryDTO;

    const findCategory = await this.categoryModel.findOne({ category });
    if (findCategory)
      throw new BadRequestException(`Category ${category} Already in use`);

    const createdCategory = new this.categoryModel(createCategoryDTO);

    return await createdCategory.save();
  }

  async findCategories(): Promise<Category[]> {
    return await this.categoryModel.find().populate('players');
  }

  async findCategoryById(categoryId: string): Promise<Category> {
    if (!mongoose.isObjectIdOrHexString(categoryId))
      throw new NotFoundException('Category Not Found');

    const categoryFound = await this.categoryModel
      .findById(categoryId)
      .populate('players');

    if (!categoryFound) throw new NotFoundException('Category Not Found');

    return categoryFound;
  }

  async findCategoryByPlayer(playerId: any): Promise<Category> {
    const players = await this.playerService.findAllPlayers();

    const playerFilter = players.filter((player) => player._id == playerId);

    if (playerFilter.length == 0) {
      throw new BadRequestException(`${playerId} it's not a player`);
    }

    return await this.categoryModel.findOne().where('players').in(playerId);
  }

  async updateCategory(
    categoryId: string,
    updateCategoryDTO: UpdateCategoryDTO,
  ): Promise<void> {
    if (!mongoose.isObjectIdOrHexString(categoryId))
      throw new NotFoundException('Category Not Found');

    const categoryFound = await this.categoryModel.findById(categoryId);

    if (!categoryFound) throw new NotFoundException('Category Not Found');

    await this.categoryModel.findOneAndUpdate(
      { _id: categoryId },
      {
        $set: updateCategoryDTO,
      },
    );
  }

  async addPlayerToCategory(params: string[]): Promise<void> {
    const category = params['category'];
    const playerId = params['playerId'];

    if (!mongoose.isObjectIdOrHexString(playerId))
      throw new NotFoundException('Player Not Found');

    const categoryFound = await this.categoryModel.findOne({ category });
    if (!categoryFound) throw new NotFoundException('Category Not Found');

    await this.playerService.findPlayerBy({
      _id: playerId,
    });

    const playersOnCategory = await this.categoryModel
      .find({ category })
      .where('players')
      .in(playerId);

    if (playersOnCategory.length > 0)
      throw new BadRequestException('Player Already on Category');

    categoryFound.players.push(playerId);
    await this.categoryModel.findOneAndUpdate(
      { category },
      { $set: categoryFound },
    );
  }
}
