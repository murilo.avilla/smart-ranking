import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ChallengesService } from './challenges.service';
import { CreateChallengeDTO } from './dtos/create-challenge.dto';
import { Challenge } from './interfaces/challenge.interface';
import { ChallengeStatusValidationPipe } from './pipes/challenge-status-validation.pipe';
import { UpdateChallengeDTO } from './dtos/update-challenge.dto';
import { AddMatchToChallengeDTO } from './dtos/add-match-challenge.dto';

@Controller('api/v1/challenges')
export class ChallengesController {
  constructor(private readonly challengeService: ChallengesService) {}

  @Post()
  @UsePipes(ValidationPipe)
  async createChallente(
    @Body() createChallengeDTO: CreateChallengeDTO,
  ): Promise<Challenge> {
    return await this.challengeService.createChallenge(createChallengeDTO);
  }

  @Get()
  async findChallenges(): Promise<Array<Challenge>> {
    return await this.challengeService.findAll();
  }

  @Get('/player/:playerId')
  async findChallengeByPlayer(
    @Param('playerId') playerId: string,
  ): Promise<Array<Challenge>> {
    return await this.challengeService.findBy({ playerId });
  }

  @Get('/:challengeId')
  async findChallengeById(
    @Param('challengeId') challengeId: string,
  ): Promise<Challenge> {
    return await this.challengeService.findById(challengeId);
  }

  @Put('/:challenge')
  async updateChallenge(
    @Body(ChallengeStatusValidationPipe) updateChallengeDTO: UpdateChallengeDTO,
    @Param('challenge') _id: string,
  ): Promise<void> {
    await this.challengeService.updateChallente(_id, updateChallengeDTO);
  }

  @Post('/:challenge/match/')
  async addMatchToChallenge(
    @Body(ValidationPipe) addMatchToChallengeDTO: AddMatchToChallengeDTO,
    @Param('challenge') _id: string,
  ): Promise<void> {
    return await this.challengeService.addMatchToChallenge(
      _id,
      addMatchToChallengeDTO,
    );
  }

  @Delete('/:challenge')
  async deleteChallenge(@Param('challenge') _id: string): Promise<void> {
    return await this.challengeService.deleteChallenge(_id);
  }
}
