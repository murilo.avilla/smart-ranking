import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { CreateChallengeDTO } from './dtos/create-challenge.dto';
import { Challenge, Match } from './interfaces/challenge.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CategoriesService } from 'src/categories/categories.service';
import { PlayersService } from 'src/players/players.service';
import { Category } from 'src/categories/interfaces/category.interface';
import { ChallengeStatus } from './interfaces/challenge-status.enum';
import { UpdateChallengeDTO } from './dtos/update-challenge.dto';
import { AddMatchToChallengeDTO } from './dtos/add-match-challenge.dto';

@Injectable()
export class ChallengesService {
  constructor(
    @InjectModel('Challenge') private readonly challengeModel: Model<Challenge>,
    @InjectModel('Match') private readonly matchModel: Model<Match>,
    private readonly categoriesService: CategoriesService,
    private readonly playersService: PlayersService,
  ) {}

  async findBy(filter: any): Promise<Array<Challenge>> {
    return await this.challengeModel
      .where('players')
      .in(filter.playerId)
      .populate('players')
      .populate('requester')
      .populate('match');
  }

  async findById(challengeId: string): Promise<Challenge> {
    return await this.challengeModel
      .findOne({ _id: challengeId })
      .populate('players')
      .populate('requester')
      .populate('match');
  }

  async findAll(): Promise<Challenge[]> {
    return await this.challengeModel
      .find()
      .populate('players')
      .populate('requester')
      .populate('match');
  }
  async createChallenge(
    createChallengeDTO: CreateChallengeDTO,
  ): Promise<Challenge> {
    const players = await this.playersService.findAllPlayers();

    createChallengeDTO.players.map((player) => {
      const playerFilter = players.filter(
        (playerDb) => playerDb._id == player._id,
      );

      if (playerFilter.length === 0)
        throw new BadRequestException(
          `Player ${player._id} not found on database`,
        );
    });

    const requesterPlayer = await createChallengeDTO.players.filter(
      (player) => player._id == createChallengeDTO.requester,
    );

    if (requesterPlayer.length == 0)
      throw new BadRequestException('Requester must be a player');

    const requesterCategory: Category =
      await this.categoriesService.findCategoryByPlayer(
        createChallengeDTO.requester,
      );

    if (!requesterCategory)
      throw new BadRequestException('Requester must be in a category');

    const challenge = new this.challengeModel(createChallengeDTO);
    challenge.category = requesterCategory.category;
    challenge.dateTimeRequest = new Date();
    challenge.status = ChallengeStatus.PENDING;

    return await challenge.save();
  }

  async updateChallente(
    _id: string,
    updateChallengeDTO: UpdateChallengeDTO,
  ): Promise<void> {
    const challengeFound = await this.challengeModel.findById(_id);

    if (!challengeFound) throw new NotFoundException('Challenge not found');

    if (updateChallengeDTO.status) challengeFound.dateTimeResponse = new Date();

    challengeFound.status = updateChallengeDTO.status;
    challengeFound.dateTimeChallenge = updateChallengeDTO.dateTimeChallenge;

    await this.challengeModel.findOneAndUpdate(
      { _id },
      { $set: challengeFound },
    );
  }

  async addMatchToChallenge(
    _id: string,
    addMatchToChallengeDTO: AddMatchToChallengeDTO,
  ): Promise<void> {
    const challengeFound = await this.challengeModel.findById(_id);

    if (!challengeFound) throw new BadRequestException('Challenge not found');

    const playerFilter = challengeFound.players.filter(
      (player) => player._id == addMatchToChallengeDTO.def,
    );

    if (playerFilter.length == 0)
      throw new BadRequestException('Winner not in challenge');

    const createdMatch = new this.matchModel(addMatchToChallengeDTO);

    createdMatch.category = challengeFound.category;

    createdMatch.players = challengeFound.players;

    const result = await createdMatch.save();

    challengeFound.status = ChallengeStatus.REALIZED;

    challengeFound.match = result._id;

    try {
      await this.challengeModel.findOneAndUpdate(
        { _id },
        { $set: challengeFound },
      );
    } catch (error) {
      await this.challengeModel.deleteOne({ _id: result._id });
      throw new InternalServerErrorException();
    }
  }

  async deleteChallenge(_id: string): Promise<void> {
    const challengeFound = await this.challengeModel.findById(_id);

    if (!challengeFound) throw new BadRequestException('Challenge not foud');

    challengeFound.status = ChallengeStatus.CANCELED;

    await this.challengeModel.findOneAndUpdate(
      { _id },
      { $set: challengeFound },
    );
  }
}
