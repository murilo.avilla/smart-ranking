import { IsIn, IsOptional } from 'class-validator';
import { ChallengeStatus } from '../interfaces/challenge-status.enum';

export class UpdateChallengeDTO {
  @IsOptional()
  dateTimeChallenge: Date;

  @IsIn([
    ChallengeStatus.DENIED,
    ChallengeStatus.REALIZED,
    ChallengeStatus.ACCEPTED,
    ChallengeStatus.CANCELED,
  ])
  status: ChallengeStatus;

  @IsOptional()
  dateTimeResponse: Date;
}
